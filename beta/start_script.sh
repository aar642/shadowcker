#!/bin/sh

systemctl --user import-environment DISPLAY XAUTHORITY

if which dbus-update-activation-environment >/dev/null 2>&1; then
   dbus-update-activation-environment DISPLAY XAUTHORITY
fi

if [ "$(getconf LONG_BIT)" -ne 64 ]; then 
   echo "Docker environement needs to be 64bit, reinstall Docker and ensure your local machine is x86_64, exiting"; 
   exit 1;
else
   echo "Docker environement is 64bit, proceed";
fi

## Uncomment this for appimage patching; since appimages are self-updating, it needs to be done each start
#rm -rf ./squashfs-root/
#wget https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage --no-check-certificate -O ./appimagetool.AppImage
#chmod +x ./appimagetool.AppImage
#/home/shadow-user/Shadow.AppImage --appimage-extract

## Do your magic here
#cp ./gamecontrollerdb.txt ./squashfs-root/resources/app.asar.unpacked/release/native/gamecontrollerdb.txt

## Rebuilds the image
#./appimagetool.AppImage ./squashfs-root/
#cp Shadow_Alpha-x86_64.AppImage Shadow.AppImage

$1
echo Exiting Shadowcker...
